// Copyright (C) 2016, Lefteris Zafiris <zaf@fastmail.com>
// This program is free software, distributed under the terms of
// the BSD 3-Clause License. See the LICENSE file
// at the top of the source tree.

package g711

const (
	ulawMax  int16 = 0x1fff
	ulawBias int16 = 33
	alawMax  int16 = 0xfff
)

// u-law to A-law conversions
var u2a = [128]uint8{
	1, 1, 2, 2, 3, 3, 4, 4,
	5, 5, 6, 6, 7, 7, 8, 8,
	9, 10, 11, 12, 13, 14, 15, 16,
	17, 18, 19, 20, 21, 22, 23, 24,
	25, 27, 29, 31, 33, 34, 35, 36,
	37, 38, 39, 40, 41, 42, 43, 44,
	46, 48, 49, 50, 51, 52, 53, 54,
	55, 56, 57, 58, 59, 60, 61, 62,
	64, 65, 66, 67, 68, 69, 70, 71,
	72, 73, 74, 75, 76, 77, 78, 79,
	80, 82, 83, 84, 85, 86, 87, 88,
	89, 90, 91, 92, 93, 94, 95, 96,
	97, 98, 99, 100, 101, 102, 103, 104,
	105, 106, 107, 108, 109, 110, 111, 112,
	113, 114, 115, 116, 117, 118, 119, 120,
	121, 122, 123, 124, 125, 126, 127, 128,
}

// A-law to u-law conversions
var a2u = [128]uint8{
	1, 3, 5, 7, 9, 11, 13, 15,
	16, 17, 18, 19, 20, 21, 22, 23,
	24, 25, 26, 27, 28, 29, 30, 31,
	32, 32, 33, 33, 34, 34, 35, 35,
	36, 37, 38, 39, 40, 41, 42, 43,
	44, 45, 46, 47, 48, 48, 49, 49,
	50, 51, 52, 53, 54, 55, 56, 57,
	58, 59, 60, 61, 62, 63, 64, 64,
	65, 66, 67, 68, 69, 70, 71, 72,
	73, 74, 75, 76, 77, 78, 79, 80,
	80, 81, 82, 83, 84, 85, 86, 87,
	88, 89, 90, 91, 92, 93, 94, 95,
	96, 97, 98, 99, 100, 101, 102, 103,
	104, 105, 106, 107, 108, 109, 110, 111,
	112, 113, 114, 115, 116, 117, 118, 119,
	120, 121, 122, 123, 124, 125, 126, 127,
}

// EncodeUlaw encodes an lpcm sample to u-law
func EncodeUlaw(lpcm int16) int8 {
	var mask int16 = 0x1000
	var sign uint8 = 0
	var position uint8
	var lsb uint8 = 0

	lpcm >>= 2
	if lpcm < 0 {
		lpcm = -lpcm
		sign = 0x80
	}
	lpcm += ulawBias
	if lpcm > ulawMax {
		lpcm = ulawMax
	}
	for position = 12; (lpcm & mask) != mask && position >= 5; position-- {
		mask >>= 1
	}
	lsb = uint8(lpcm >> (position - 4)) & 0x0f
	return int8(^(sign | ((position - 5) << 4) | lsb))
}

// DecodeUlaw decodes a u-law pcm sample to lpcm
func DecodeUlaw(pcm int8) int16 {
	var sign uint8 = 0
	var position uint8 = 0
	var decoded int16 = 0

	pcm = ^pcm
	if pcm < 0 {
		pcm = -pcm
		sign = 0x80
	}
	decoded = int16((1 << position) | ((pcm & 0x0f) << (position - 4)) | (1 << (position - 5))) - ulawBias
	if sign == 0 {
		return decoded
	}
	return -decoded
}

// EncodeAlaw encodes an lpcm sample to A-law
func EncodeAlaw(lpcm int16) int8 {
	var mask int16 = 0x800
	var sign uint8 = 0
	var position uint8
	var lsb uint8 = 0

	lpcm >>= 3
	if lpcm < 0 {
		lpcm = -lpcm
		sign = 0x80
	}
	if lpcm > alawMax {
		lpcm = alawMax
	}
	for position = 11; (lpcm & mask) != mask && position >= 5; position-- {
		mask >>= 1
	}
	if position == 4 {
		lsb = uint8(lpcm >> 1) & 0x0f
	} else {
		lsb = uint8(lpcm >> (position - 4)) & 0x0f
	}
	return int8(sign | ((position - 4) << 4) | lsb) ^ 0x55
}

// DecodeAlaw decodes an A-law pcm sample to lpcm
func DecodeAlaw(pcm int8) int16 {
	var sign uint8 = 0
	var position uint8 = 0
	var decoded int16 = 0

	pcm ^= 0x55
	if pcm < 0 {
		pcm = -pcm
		sign = 0x80
	}
	position = ((uint8(pcm) & uint8(0xf0)) >> 4) + 4
	if position != 4 {
		decoded = int16((1 << position) | ((pcm & 0x0f) << (position - 4)) | (1 << (position - 5)))
	} else {
		decoded = int16((pcm << 1) | 1)
	}
	if sign == 0 {
		return decoded
	}
	return -decoded
}

// Alaw2Ulaw performs direct A-law to u-law conversion
func Alaw2Ulaw(aval int8) int8 {
	aval = int8(uint8(aval) & uint8(0xff))
	if aval > 0 {
		return int8(0x7F ^ a2u[uint8(aval) ^ uint8(0x55)])
	}
	return int8(0xFF ^ a2u[uint8(aval) ^ uint8(0xD5)])
}

// Ulaw2Alaw performs direct u-law to A-law conversion
func Ulaw2Alaw(uval int8) int8 {
	uval = int8(uint8(uval) & uint8(0xff))
	if uval > 0 {
		return int8(0x55 ^ (u2a[uint8(0x7F) ^ uint8(uval)] - 1))
	}
	return int8(0xD5 ^ (u2a[uint8(0xFF) ^ uint8(uval)] - 1))
}
