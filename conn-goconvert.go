/*
	Sound file conversion using soxlib

	usage: goconvert input.file output.file output.format

*/
package main

import (
	"io/ioutil"
	"log"
	"os"
	"sync"

	"./sox"
)

func main() {
	if len(os.Args) < 4 {
		log.Fatalln("not enough parameters")
	}
	inputFile  := os.Args[1]
	//outputFile := os.Args[2]
	outFormat  := os.Args[3]

	err := sox.Init()
	if err != nil {
		log.Fatal(err)
	}

	err = sox.FormatInit()
	if err != nil {
		log.Fatal(err)
	}

	sem := make(chan struct{}, 2)
	var wg sync.WaitGroup
	for i := 0; i < 4; i++ {
		sem <- struct{}{}
		wg.Add(1)
		go func() {
			defer wg.Done()
			log.Println("start goroutine")
			input, err := ioutil.ReadFile(inputFile)
			if err != nil {
				log.Fatalln(err)
			}
			_, err = sox.Convert(input, outFormat)
			if err != nil {
				log.Fatalln(err)
			}
			<- sem
		}()
	}
	wg.Wait()
	sox.FormatQuit()
	sox.Quit()
}
