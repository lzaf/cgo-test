/* Simple example of using SoX libraries */

#include <sox.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>

/* Example of reading and writing audio files stored in memory buffers
 * rather than actual files.
 *
 * Usage: example5 input output format
*/

typedef struct snd_file {
	void *buff;
	size_t size;
} snd_file;

snd_file convert_snd(snd_file *in_snd, char* format) {
	snd_file out_snd = { NULL, 0 };
	static sox_format_t *in, *out;
	sox_effects_chain_t *chain;
	sox_effect_t *e;
	char *args[10];
	char *ftype;
	sox_signalinfo_t interm_signal, out_signal;
	sox_encodinginfo_t out_encoding;

	if (strcmp(format, "ulaw") == 0) {
		ftype = "ul";
		out_signal = (sox_signalinfo_t) { 8000, 1, 0, 0, NULL };
		out_encoding = (sox_encodinginfo_t) {
			SOX_ENCODING_ULAW,
			8,
			0,
			sox_option_default,
			sox_option_default,
			sox_option_default,
			sox_false
		};
	} else if (strcmp(format, "alaw") == 0) {
		ftype = "al";
		out_signal = (sox_signalinfo_t) { 8000, 1, 0, 0, NULL };
		out_encoding = (sox_encodinginfo_t) {
			SOX_ENCODING_ALAW,
			8,
			0,
			sox_option_default,
			sox_option_default,
			sox_option_default,
			sox_false
		};
	} else if (strcmp(format, "gsm") == 0) {
		ftype = "gsm";
		out_signal = (sox_signalinfo_t) { 8000, 1, 0, 0, NULL };
		out_encoding = (sox_encodinginfo_t) {
			SOX_ENCODING_GSM,
			0,
			1,
			sox_option_default,
			sox_option_default,
			sox_option_default,
			sox_false
		};
	} else if (strcmp(format, "wav") == 0 || strcmp(format, "wav16") == 0) {
		ftype = "wav";
		out_signal = (sox_signalinfo_t) { 16000, 1, 0, 0, NULL };
		out_encoding = (sox_encodinginfo_t) {
			SOX_ENCODING_SIGN2,
			16,
			0,
			sox_option_default,
			sox_option_default,
			sox_option_default,
			sox_false
		};
	} else if (strcmp(format, "wav8") == 0) {
		ftype = "wav";
		out_signal = (sox_signalinfo_t) { 8000, 1, 0, 0, NULL };
		out_encoding = (sox_encodinginfo_t) {
			SOX_ENCODING_SIGN2,
			16,
			0,
			sox_option_default,
			sox_option_default,
			sox_option_default,
			sox_false
		};
	} else if (strcmp(format, "mp3") == 0) {
		ftype = "mp3";
		out_signal = (sox_signalinfo_t) { 16000, 1, 0, 0, NULL };
		out_encoding = (sox_encodinginfo_t) {
			SOX_ENCODING_MP3,
			0,
			32,
			sox_option_default,
			sox_option_default,
			sox_option_default,
			sox_false
		};
	} else if (strcmp(format, "ogg") == 0) {
		ftype = "ogg";
		out_signal = (sox_signalinfo_t) { 16000, 1, 0, 0, NULL };
		out_encoding = (sox_encodinginfo_t) {
			SOX_ENCODING_VORBIS,
			0,
			4,
			sox_option_default,
			sox_option_default,
			sox_option_default,
			sox_false
		};
	} else if (strcmp(format, "flac") == 0) {
		ftype = "flac";
		out_signal = (sox_signalinfo_t) { 16000, 1, 0, 0, NULL };
		out_encoding = (sox_encodinginfo_t) {
			SOX_ENCODING_FLAC,
			0,
			6,
			sox_option_default,
			sox_option_default,
			sox_option_default,
			sox_false
		};
	} else {
		return out_snd;
	}

	if (sox_init() != SOX_SUCCESS || sox_format_init() != SOX_SUCCESS) {
		return out_snd;
	}

	in = sox_open_mem_read(in_snd->buff, in_snd->size, NULL, NULL, NULL);
	if (in == NULL || in->encoding.encoding == SOX_ENCODING_UNKNOWN ) {
		return out_snd;
	}
	//printf("%s, %d, %p, %p\n", ftype, in_snd->size, in_snd->buff, format);
	out = sox_open_memstream_write((char**)&out_snd.buff, &out_snd.size, &out_signal, &out_encoding, ftype, NULL);
	if (out == NULL) {
		sox_close(in);
		return out_snd;
	}
	//printf("%s, %d, %p, %p\n", ftype, out_snd.size, out_snd.buff, format);
	chain = sox_create_effects_chain(&in->encoding, &out->encoding);

	interm_signal = in->signal;

	e = sox_create_effect(sox_find_effect("input"));
	args[0] = (char *) in, sox_effect_options(e, 1, args);
	sox_add_effect(chain, e, &interm_signal, &in->signal);
	free(e);

	if (in->signal.rate != out->signal.rate) {
		e = sox_create_effect(sox_find_effect("rate"));
		sox_effect_options(e, 0, NULL);
		sox_add_effect(chain, e, &interm_signal, &out->signal);
		free(e);
	}

	if (in->signal.channels != out->signal.channels) {
		e = sox_create_effect(sox_find_effect("channels"));
		sox_effect_options(e, 0, NULL);
		sox_add_effect(chain, e, &interm_signal, &out->signal);
		free(e);
	}

	e = sox_create_effect(sox_find_effect("output"));
	args[0] = (char *) out, sox_effect_options(e, 1, args);
	sox_add_effect(chain, e, &interm_signal, &out->signal);
	free(e);

	sox_flow_effects(chain, NULL, NULL);

	sox_delete_effects_chain(chain);
	sox_close(in);
	sox_close(out);
	sox_format_quit();
	sox_quit();

	return out_snd;
}

int main(int argc, char *argv[]) {
	snd_file in, out;
	if (argc != 4) {
		printf("Not enough parameters\n");
		return 1;
	}

	FILE *f = fopen(argv[1], "rb");
	if (!f) {
		printf("Failed to open file: %s\n", argv[1]);
		return 1;
	}
	fseek(f, 0, SEEK_END);
	in.size = ftell(f);
	fseek(f, 0, SEEK_SET);
	in.buff = malloc(in.size);
	fread(in.buff, in.size, 1, f);
	fclose(f);
	int i;
	for (i=0; i<10; i++) {
		out = convert_snd(&in, argv[3]);
		if (out.size == 0) {
			printf("Failed to convert file: %s\n", argv[1]);
			return 1;
		}
	}
	f = fopen(argv[2],"wb");
	if (!f) {
		printf("Failed to open file: %s\n", argv[2]);
		return 1;
	}
	fwrite(out.buff, out.size, 1, f);
	fclose(f);

	free(in.buff);
	free(out.buff);
	return 0;
}
