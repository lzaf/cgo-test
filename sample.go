/*
	Sound file resampling
*/
package main

import (
	"io/ioutil"
	"log"
	"os"

	"./resample"
)

func main() {
	if len(os.Args) < 3 {
		log.Fatalln("not enough parameters")
	}
	inputFile := os.Args[1]
	outputFile := os.Args[2]
	var err error
	var input []byte

	input, err = ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatalln(err)
	}

	var sndIn resample.SndData
	sndIn.Data = input //skip the WAV header
	sndIn.Rate = 16000
	sndIn.Channels = 1
	sndIn.Bits = 16
	sndIn.Type = "raw"

	sndOut, err := resample.Process(sndIn, 8000, resample.SincMediumQuality)
	if err != nil {
		log.Fatalln(err)
	}

	err = ioutil.WriteFile(outputFile, sndOut.Data, 0644)
	if err != nil {
		log.Fatalln(err)
	}
}
