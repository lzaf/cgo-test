// Copyright (C) 2016, Lefteris Zafiris <zaf@fastmail.com>
// This program is free software, distributed under the terms of
// the BSD 3-Clause License. See the LICENSE file
// at the top of the source tree.

package resample

/*
#cgo LDFLAGS: -lsamplerate

#include <stdio.h>
#include <stdlib.h>
#include <samplerate.h>
*/
import "C"

import (
	"fmt"
	"unsafe"
)

const (
	SincBestQuality = iota
	SincMediumQuality
	SincFAastest
	ZeroOrderHold
	Linear
	sizeOfFloat = 4
	sizeOfShort = 2
	byteLen     = 8
)

//LPCM data
type SndData struct {
	Data     []byte
	Rate     int
	Channels int
	Bits     int
	Type     string
}

//Resample LPCM data
func Process(source SndData, rate int, method int) (out SndData, err error) {
	var srcData C.SRC_DATA

	framesIn := len(source.Data) / source.Channels / (source.Bits / byteLen)
	framesOut := framesIn * rate / source.Rate

	dataIn := C.malloc(C.size_t(len(source.Data)))
	cBuf := (*[1 << 30]byte)(dataIn)
	copy(cBuf[:], source.Data)
	srcData.data_in = (*C.float)(C.malloc(C.size_t(framesIn * sizeOfFloat)))
	C.src_short_to_float_array((*C.short)(dataIn), srcData.data_in, C.int(len(source.Data)/sizeOfShort))
	srcData.data_out = (*C.float)(C.malloc(C.size_t(framesOut * sizeOfFloat)))
	srcData.input_frames = C.long(framesIn)
	srcData.output_frames = C.long(framesOut)
	srcData.src_ratio = C.double(float64(rate) / float64(source.Rate))

	errCode := C.src_simple(&srcData, C.int(method), C.int(source.Channels))
	if int(errCode) != 0 {
		errMsg := C.src_strerror(errCode)
		err = fmt.Errorf(C.GoString(errMsg))
		C.free(unsafe.Pointer(errMsg))
		C.free(unsafe.Pointer(dataIn))
		C.free(unsafe.Pointer(srcData.data_in))
		C.free(unsafe.Pointer(srcData.data_out))
		return
	}

	outBuf := C.malloc(C.size_t(framesOut * sizeOfFloat))
	C.src_float_to_short_array((*C.float)(srcData.data_out), (*C.short)(outBuf), C.int(framesOut))

	out.Type = source.Type
	out.Rate = rate
	out.Channels = source.Channels
	out.Bits = source.Bits
	out.Data = C.GoBytes(unsafe.Pointer(outBuf), C.int(framesOut*out.Channels*out.Bits/byteLen))
	C.free(unsafe.Pointer(dataIn))
	C.free(unsafe.Pointer(srcData.data_in))
	C.free(unsafe.Pointer(srcData.data_out))
	C.free(unsafe.Pointer(outBuf))
	return
}
