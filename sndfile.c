/* Simple example of using SoX libraries */

#include <samplerate.h>
#include <sndfile.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>

#define RAW_PCM_S16LE 262146
#define WAV_PCM_S16LE 65538


int main(int argc, char *argv[]) {
	int res;
	float *src_buff, *dst_buff;
	long dst_frames;
	SRC_DATA data;
	SNDFILE *src_file, *dst_file;
	SF_INFO src_info, dst_info;

	if (argc != 3) {
		printf("Not enough parameters\n");
		return 1;
	}

	src_info.samplerate = 16000;
	src_info.channels = 1;
	src_info.format = RAW_PCM_S16LE;
	if ( (src_file = sf_open(argv[1], SFM_READ, &src_info)) == NULL ) {
		printf("Failed to open file: %s\n", argv[1]);
		return 1;
	}

	src_buff = (float *)malloc(src_info.frames * sizeof(float));
	sf_readf_float(src_file, src_buff, src_info.frames);

	dst_frames = src_info.frames * 8000 / 16000;

	dst_buff = (float *)malloc(dst_frames * sizeof(float));

	data.data_in = src_buff;
	data.data_out = dst_buff;
	data.input_frames = src_info.frames;
	data.output_frames = dst_frames;
	data.src_ratio = 8000.0 / 16000.0;

	printf("%ld %ld\n", data.input_frames,  data.output_frames);

	res = src_simple(&data, 0, 1);
	if (res != 0) {
		printf("Failed to convert file: %s\n", argv[1]);
		return 1;
	}
	dst_info.samplerate = 8000;
	dst_info.channels = 1;
	dst_info.format = RAW_PCM_S16LE;
	if ( (dst_file = sf_open(argv[2], SFM_WRITE, &dst_info)) == NULL ) {
		printf("Failed to open file: %s\n", argv[1]);
		return 1;
	}
	sf_writef_float(dst_file, dst_buff, dst_frames);
	sf_write_sync(dst_file);
	sf_close(src_file);
	sf_close(dst_file);
	free(src_buff);
	free(dst_buff);

	return 0;
}
