/* Simple example of using SoX libraries */

#include <sox.h>
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>

#define MAX_SAMPLES (size_t)2048

/* Example of reading and writing audio files stored in memory buffers
 * rather than actual files.
 *
 * Usage: example5 input output
 */

size_t au_file_read(char **buff, char *file)
{
	sox_format_t *in, *out;
	sox_sample_t samples[MAX_SAMPLES];
	size_t buffer_size = 0, number_read = 0;
	char *ftype = "sox";

	if (sox_init() != SOX_SUCCESS) {
		return 0;
	}

	in = sox_open_read(file, NULL, NULL, NULL);
	//printf("%d %f\n", in->encoding.bits_per_sample, in->encoding.compression);
	out = sox_open_memstream_write(buff, &buffer_size, &in->signal, NULL, ftype, NULL);
	while ((number_read = sox_read(in, samples, MAX_SAMPLES))) {
		if (sox_write(out, samples, number_read) != number_read) {
			return 0;
		}
	}
	sox_close(out);
	sox_close(in);
	sox_quit();
	return buffer_size;
}

int au_file_write(char **buff, size_t buffer_size, char *file)
{
	sox_format_t *in, *out;
	sox_sample_t samples[MAX_SAMPLES];
	size_t number_read = 0;
	char *ftype = "ul";

	if (sox_init() != SOX_SUCCESS) {
		return 0;
	}

	in = sox_open_mem_read(*buff, buffer_size, NULL, NULL, ftype);
	out = sox_open_write(file, &in->signal, NULL, NULL, NULL, NULL);
	while ((number_read = sox_read(in, samples, MAX_SAMPLES))) {
		if (sox_write(out, samples, number_read) != number_read) {
			return 0;
		}
	}
	sox_close(out);
	sox_close(in);
	sox_quit();
	return 1;
}

size_t convert_ulaw(char **read_buff, size_t r_buffer_size, char **write_buff)
{
	static sox_format_t *in, *out;
	size_t w_buffer_size = 0;
	sox_effects_chain_t *chain;
	sox_effect_t *e;
	char *args[10];
	char *ftype = "ul";
	sox_signalinfo_t interm_signal;

	sox_encodinginfo_t out_encoding = {
		SOX_ENCODING_ULAW,
		8,
		0,
		sox_option_default,
		sox_option_default,
		sox_option_default,
		sox_false
	};

	sox_signalinfo_t out_signal = {
		8000,
		1,
		0,
		0,
		NULL
	};

	if (sox_init() != SOX_SUCCESS) {
		return 0;
	}
	in = sox_open_mem_read(*read_buff, r_buffer_size, NULL, NULL, NULL);
	out = sox_open_memstream_write(write_buff, &w_buffer_size, &out_signal, &out_encoding, ftype, NULL);

	chain = sox_create_effects_chain(&in->encoding, &out->encoding);

	interm_signal = in->signal;

	e = sox_create_effect(sox_find_effect("input"));
	args[0] = (char *) in, sox_effect_options(e, 1, args);
	sox_add_effect(chain, e, &interm_signal, &in->signal);
	free(e);

	if (in->signal.rate != out->signal.rate) {
		e = sox_create_effect(sox_find_effect("rate"));
		sox_effect_options(e, 0, NULL);
		sox_add_effect(chain, e, &interm_signal, &out->signal);
		free(e);
	}

	if (in->signal.channels != out->signal.channels) {
		e = sox_create_effect(sox_find_effect("channels"));
		sox_effect_options(e, 0, NULL);
		sox_add_effect(chain, e, &interm_signal, &out->signal);
		free(e);
	}

	e = sox_create_effect(sox_find_effect("output"));
	args[0] = (char *) out, sox_effect_options(e, 1, args);
	sox_add_effect(chain, e, &interm_signal, &out->signal);
	free(e);

	sox_flow_effects(chain, NULL, NULL);

	sox_delete_effects_chain(chain);
	sox_close(out);
	sox_close(in);
	sox_quit();

	return w_buffer_size;
}

int main(int argc, char *argv[])
{
	char *r_buf, *w_buf;
	size_t r_size = 0, w_size = 0;
	if (argc != 3) {
		return 1;
	}

	r_size = au_file_read(&r_buf, argv[1]);
	if (r_size == 0) {
		return 1;
	}

	w_size = convert_ulaw(&r_buf, r_size, &w_buf);
	if (w_size == 0) {
		return 1;
	}

	if (!au_file_write(&w_buf, w_size, argv[2])) {
		return 1;
	}

	free(r_buf);
	free(w_buf);
	return 0;
}
