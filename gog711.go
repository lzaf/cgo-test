/*
	Sound file conversion using g711
*/
package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"os"

	"./g711"
)

func main() {
	if len(os.Args) < 3 {
		log.Fatalln("not enough parameters")
	}
	inputFile := os.Args[1]
	outputFile := os.Args[2]
	var err error
	var input []byte
	var buf bytes.Buffer

	input, err = ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatalln(err)
	}

	//Read WAV header
	// 	for i:=0; i<42; i+=2 {
	// 		log.Printf("%v %v\n", string(input[i]), string(input[i+1]))
	// 		log.Printf("%v\n", int16( int16(input[i]) | int16(input[i+1])<<8))
	// 	}

	//Skip WAV header and read pcm data
	for i := 44; i <= len(input)-2; i += 2 {
		buf.WriteByte(byte(g711.EncodeUlaw(int16(input[i]) | int16(input[i+1]) << 8)))
	}

	err = ioutil.WriteFile(outputFile, buf.Bytes(), 0644)
	if err != nil {
		log.Fatalln(err)
	}
}
