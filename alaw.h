#include <stdint.h>

inline int16_t val_seg(int16_t val) {
  int16_t r = 1;

  val >>= 8;
  if (val & 0xf0) {
    val >>= 4;
    r += 4;
  }
  if (val & 0x0c) {
    val >>= 2;
    r += 2;
  }
  if (val & 0x02)
    r += 1;
  return r;
}

int8_t s16_to_alaw(int16_t pcm_val) {
  int16_t seg;
  uint8_t mask;
  uint8_t aval;

  if (pcm_val >= 0) {
    mask = 0xD5;
  } else {
    mask = 0x55;
    pcm_val = -pcm_val;
    if (pcm_val > 0x7fff)
      pcm_val = 0x7fff;
  }

  if (pcm_val < 256) {
    aval = pcm_val >> 4;
  } else {
    /* Convert the scaled magnitude to segment number. */
    seg = val_seg (pcm_val);
    aval = (seg << 4) | ((pcm_val >> (seg + 3)) & 0x0f);
  }
  return aval ^ mask;
}
