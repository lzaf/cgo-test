/*
	Sound file conversion using g711
*/
package main

/*

#cgo CFLAGS: -O2 -march=native

#include "g711.h"

*/
import "C"

import (
	"bytes"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	if len(os.Args) < 3 {
		log.Fatalln("not enough parameters")
	}
	inputFile := os.Args[1]
	outputFile := os.Args[2]
	var err error
	var input []byte
	var buf bytes.Buffer

	input, err = ioutil.ReadFile(inputFile)
	if err != nil {
		log.Fatalln(err)
	}

	//Skip WAV header and read pcm data
	for i := 44; i <= len(input)-2; i += 2 {
		buf.WriteByte(byte(C.MuLaw_Encode(C.int16_t(int16(input[i]) | int16(input[i+1]) << 8))))
	}

	err = ioutil.WriteFile(outputFile, buf.Bytes(), 0644)
	if err != nil {
		log.Fatalln(err)
	}
}
