package main

/*
#cgo LDFLAGS: -lflite_cmu_us_kal16 -lflite

#include "gflite.h"
*/
import "C"

import "unsafe"

func main() {
	text := C.CString("Hello there stranger! This is a simple test.")
	C.flitePLayback(text)
	C.free(unsafe.Pointer(text))
}
