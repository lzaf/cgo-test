#include <flite/flite.h>

cst_voice *register_cmu_us_kal16(void);
void unregister_cmu_us_kal16(cst_voice *v);

int flitePLayback(char *text) {
	cst_voice *voice;
	flite_init();
	voice = register_cmu_us_kal16();
	flite_text_to_speech(text,voice,"play");
	unregister_cmu_us_kal16(voice);
	return 0;
}
