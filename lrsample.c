/* Simple example of using libsamplerate */

#include <samplerate.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <sys/stat.h>


int main(int argc, char *argv[]) {
	int in_size, res;
	struct stat st;
	short *in_buff, *out_buff;
	long in_frames, out_frames;
	SRC_DATA data;
	float *in, *out;

	if (argc != 3) {
		printf("Not enough parameters\n");
		return 1;
	}

	FILE *f = fopen(argv[1], "rb");
	if (!f) {
		printf("Failed to open file: %s\n", argv[1]);
		return 1;
	}
	stat(argv[1], &st);
	in_size = st.st_size;
	in_buff = malloc(in_size);
	fread(in_buff, in_size, 1, f);
	fclose(f);

	in_frames = in_size / 2;
	in = (float *)(malloc(in_frames * sizeof(float)));
	if (in == NULL) {
		printf("Failed to allocate memory\n");
		return 1;
	}
	src_short_to_float_array(in_buff, in, in_size/sizeof(short));
	out_frames = in_frames * 8000.0 / 16000.0;
	out = (float *)(malloc(out_frames * sizeof(float)));
	if (out == NULL) {
		printf("Failed to allocate memory\n");
		return 1;
	}
	data.data_in = in;
	data.data_out = out;
	data.input_frames = in_frames;
	data.output_frames = out_frames;
	data.src_ratio = 8000.0 / 16000.0;

	//printf("%ld %ld %d %d %d\n", data.input_frames,  data.output_frames, sizeof(float), sizeof(int), sizeof(short));

	res = src_simple(&data, 0, 1);
	if (res != 0) {
		printf("Failed to convert file: %s\n", argv[1]);
		return 1;
	}


	f = fopen(argv[2],"wb");
	if (!f) {
		printf("Failed to open file: %s\n", argv[2]);
		return 1;
	}
	out_buff = malloc(out_frames*sizeof(float));
	if (out_buff == NULL) {
		printf("Failed to allocate memory\n");
		return 1;
	}
	src_float_to_short_array(data.data_out, out_buff, out_frames);
	fwrite(out_buff, out_frames, 2, f);
	fclose(f);
	free(out_buff);
	free(in_buff);
	free(data.data_out);
	return 0;
}
